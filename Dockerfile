FROM ubuntu
RUN apt-get update && apt-get -y install g++ cmake;
COPY .  /usr/src/calculator
WORKDIR /usr/src/calculator
RUN cmake .;  make ; 
CMD ["./test/Calculator_tst"]

